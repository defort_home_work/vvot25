variable "tgkey" {
  type        = string
  description = "Telegram Bot API Key"
}

variable "YDB_DATABASE" {
  type        = string
  description = "YDB_DATABASE"
}

variable "API_GATEWAY_KEY" {
  type        = string
  description = "API_GATEWAY_KEY"
}

variable "YDB_ENDPOINT" {
  type        = string
  description = "YDB_ENDPOINT"
}

variable "service_acc_id" {
  type        = string
  description = "service_acc_id"
}

variable "bucket_size" {
  type        = number
  description = "bucket_size"
}

variable "default_region" {
  type        = string
  description = "default_region"
}
